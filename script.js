const http = require("http");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((request, response) => {
    if (request.method === "GET" && request.url === "/html") {
        response.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
        response.end();
    } else if (request.method === "GET" && request.url === "/json") {
        response.write(`{
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }`);
        response.end();
    } else if (request.method === "GET" && request.url === "/uuid") {
        const uuid = {
            uuid: uuidv4(),
        };
        response.write(JSON.stringify(uuid, null, 4));
        response.end();
    } else if (request.method === "GET" && request.url.split("/")[1] === "status") {
        const statusCode = parseInt(request.url.split("/")[2]);
        if(http.STATUS_CODES[statusCode]) {
            response.write(statusCode + ' : ' + http.STATUS_CODES[statusCode]);
            response.end();
        } else {
            response.write('Status code provided is not correct');
            response.end();
        }
    } else if (request.method === "GET" && request.url.split("/")[1] === "delay") {
        const timeDelay = parseInt(request.url.split("/")[2]);
        if(timeDelay >= 0) {
            setTimeout(() => {
                response.write('200');
                response.end();
            }, timeDelay * 1000);
        } else {
            response.write('delay time is not correct');
            response.end();
        }
    } else {
        response.write(`Bad Request`);
        response.end();
    }
});

const port = process.env.PORT || 8080;
server.listen(port, (err) => {
    if(err) {
        console.error(err);
    }
});
